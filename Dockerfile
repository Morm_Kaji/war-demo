FROM tomcat:9.0.53
RUN rm -rf /usr/share/tomcat/webapps/*
COPY target/ROOT.war /usr/share/tomcat/webapps/
CMD ["catalina.sh","run"]
